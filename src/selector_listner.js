define('Buri/dom/live', ['Buri/dom'], function(dom){
  "use strict";
  if (!window.getComputedStyle(document.documentElement, '')) {
    return;
  }
  var 
    doc = document,
    events = {},
    selectors = {},
    styles = doc.createElement('style'),
    keyframes = doc.createElement('style'),
    head = doc.getElementsByTagName('head')[0],
    startNames = ['animationstart', 'oAnimationStart', 'MSAnimationStart', 'webkitAnimationStart'],
    startEvent = function(event){
      event.selector = (events[event.animationName] || {}).selector;
      ((this.selectorListeners || {})[event.animationName] || []).forEach(function(fn){
        fn.call(this, event);
      }, this);
    },
    prefix = (function() {
      var duration = 'animation-duration: 0.01s;',
        name = 'animation-name: SelectorListener !important;',
        computed = window.getComputedStyle(doc.documentElement, ''),
        pre = (Array.prototype.slice.call(computed).join('').match(/moz|webkit|ms/)||(computed.OLink===''&&['o']))[0];
      return {
        css: '-' + pre + '-',
        properties: '{' + duration + name + '-' + pre + '-' + duration + '-' + pre + '-' + name + '}',
        keyframes: !!(window.CSSKeyframesRule || window[('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1] + 'CSSKeyframesRule'])
      };
    })();
    
  styles.type = keyframes.type = "text/css";
  head.appendChild(styles);
  head.appendChild(keyframes);
  
  dom.on = function(rootNode, selector, fn){
    var key = selectors[selector],
      listeners = rootNode.selectorListeners = rootNode.selectorListeners || {};
      
    if (key) {
      events[key].count++;
    }
    else {
      key = selectors[selector] = 'SelectorListener-' + new Date().getTime();
      var node = doc.createTextNode('@' + (prefix.keyframes ? prefix.css : '') + 'keyframes ' + key + ' {' +
        'from { clip: rect(1px, auto, auto, auto); } to { clip: rect(0px, auto, auto, auto); }' +
       '}');
      keyframes.appendChild(node);
      styles.sheet.insertRule(selector + prefix.properties.replace(/SelectorListener/g, key), 0);
      events[key] = { count: 1, selector: selector, keyframe: node, rule: styles.sheet.cssRules[0] };
    } 
    
    if (listeners.count) {
      listeners.count++;
    }
    else {
      listeners.count = 1;
      startNames.forEach(function(name){
        rootNode.addEventListener(name, startEvent, false);
      });
    }
    
    (listeners[key] = listeners[key] || []).push(fn);
  };
  
  dom.off = function(rootNode, selector, fn){
    var listeners = rootNode.selectorListeners || {},
      key = selectors[selector],
      listener = listeners[key] || [],
      index = listener.indexOf(fn);
      
    if (index > -1){
      var event = events[selectors[selector]];
      event.count--;
      if (!event.count){
        styles.sheet.deleteRule(styles.sheet.cssRules.item(event.rule));
        keyframes.removeChild(event.keyframe);
        delete events[key];
        delete selectors[selector];
      }
      
      listeners.count--;
      listener.splice(index, 1);
      if (!listeners.count) {
        startNames.forEach(function(name){
          rootNode.removeEventListener(name, startEvent, false);
        });
      }
    }
  };
});