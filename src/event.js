define('Buri/event',['Buri/util','Buri/dom'], function(util, dom) {
  "use strict";
  var
    id=1,
    EVENTIGNORE = 'BuriDelegateIgnore',
    listeners = [],
    working,
    handleDelegate = function(e) {
      var
        root = this,
        listenerList = listeners[root.delegatId][e.type],
        l = listenerList.length,
        target = e.target,
        listener,
        returned,
        i;

      if (e[EVENTIGNORE] === true) {
        return;
      }
      while (target && l) {
        for (i = 0; i < l; i++) {
          listener = listenerList[i];

          if (!listener) {
            break;
          }
          if (dom.match(target, listener.selector)) {
            returned = listener.handler.call(target, e, target);
          }
          if (returned === false) {
            e[EVENTIGNORE] = true;
            return;
          }
        }

        if (target === root) {
          break;
        }
        l = listenerList.length;
        target = target.parentElement;
      }
    },
    addEvent = function(target, eventName, handler){
      if (eventName.indexOf(':relay')!==-1) {
        if (working) {
          setTimeout(function(){
            addEvent(target, eventName, handler);
          },10);
          return ;
        }
        working = true;
        eventName = eventName.split(':');
        if (!target.delegatId) {
          target.delegatId = id++;
          listeners[target.delegatId] = {};
        }
        if (!listeners[target.delegatId]) {
          listeners[target.delegatId] = {};
        }
        if (!listeners[target.delegatId][eventName[0]]) {
          listeners[target.delegatId][eventName[0]] = [];
          if (target.addEventListener) {
            target.addEventListener(eventName[0], handleDelegate, false);
          } else {
            target.attachEvent(eventName[0], handleDelegate);
          }

        }
        listeners[target.delegatId][eventName[0]].push({
          selector: eventName[1].substr(6, eventName[1].length-7),
          handler : handler,
          event: eventName[0]
        });
        working = false;
      } else if (eventName.indexOf(':debounce')!==-1) {
        var parts = eventName.split(':');
        handler = util.debounce(handler, parseInt(eventName.match(/\(([0-9]+)\)/)[1]), true);
        addEvent(parts[0], eventName, handler);
      } else if (eventName.indexOf(':once')!==-1 ) {
        var onceHandler = function(e) {
          event.off(target, eventName.split(':')[0], onceHandler);
          handler.apply(this, [e]);
        };
        addEvent(target, eventName.split(':')[0], onceHandler);
      } else {
        if (target.addEventListener) {
          target.addEventListener(eventName, handler, false);
        } else {
          target.attachEvent(eventName, handler);
        }
      }
    },
    event = {
      on: addEvent,
      off: function(target, eventName, handler) {
        if (target.removeEventListener) {
          target.removeEventListener(eventName, handler, false);
        } else {
          target.detachEvent(eventName, handler);
        }
      },
      getTarget: function(e) {
        return e.target;
      },
      preventDefault: function(event){
        if (event.preventDefault){
          event.preventDefault();
        } else {
          event.returnValue = false;
        }
      },
      extendObject: function(obj) {
        var listeners = {};
        obj.trigger = function(event, args) {
          if (!this._eid) {
            return ;
          }
          if (!listeners[this._eid][event] || listeners[this._eid][event].length === 0) {
            return ;
          }
          listeners[this._eid][event].forEach(function(clb){
            setTimeout(function(){
              clb.apply(this, args);
            }.bind(this), 0);
          }, this);
        };
        obj.on = function(event, clb) {
          if (!this._eid) {
            this._eid = id++;
          }
          if (!listeners[this._eid]) {
            listeners[this._eid] = {};
          }
          if (!listeners[this._eid][event]) {
            listeners[this._eid][event] = [];
          }
          listeners[this._eid][event].push(clb);
          return this;
        };
        return obj;
      }
    };

  return event;
});