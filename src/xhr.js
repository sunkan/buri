define('Buri/xhr', ['Buri/util', 'Buri/promise'], function(util, Promise) {
  "use strict";
  var 
    xhr = function(url, options) {
      options = options || {};
      var
        promise = new Promise(),
        req    = new XMLHttpRequest(),
        method = options.method || 'get',
        async  = (typeof options.async != 'undefined' ? options.async : true),
        params = options.data || null,
        running = false,
        handleResp = function() {
          var 
            format = options.format||'text',
            response = req.responseText;

          if (format.toLowerCase()=='json') {
            promise.resolve(JSON.parse(response));
          } else if (format.toLowerCase()=='html') {
            try {
              return promise.resolve(( new window.DOMParser() ).parseFromString( response, "text/html" ));
            } catch ( e ) {
              return promise.reject(response);
            }
          } else if (format.toLowerCase()=='jsend') {
            try {
              response = JSON.parse(response);
            } catch (e) {
              return promise.reject(response);
            }
            if (response && response.status == 'success') {
              return promise.resolve({data:response.data, response: response});
            } else {
              return promise.reject(response);
            }
          } else {
            return promise.resolve({data:req.responseText, response:req});
          }
        },
        handleProgress = function(e) {
          var percentComplete = 0;
          if (e.lengthComputable) {
            percentComplete = e.loaded / e.total;
          }
          if (options.progress) {
            options.progress({
              percent: percentComplete,
              event : e
            });
          }
        },
        handleError = (options.error && typeof options.error == 'function') ? options.error : function () {},
        send = function(data) {
          if (data && typeof data == 'object') {
            data = util.toQueryString(data);
          }
          if (data && method=='get') {
            url = url.replace(/\?(.*)+/,'?$1'+'&'+data);
          }
          try {
            req.open(method, url, async);
          }
          catch(err) {
            console.log('ERROR', err);
          }
          req.setRequestHeader('X-Requested-With','XMLHttpRequest');

          if (method.toLowerCase() == 'post') {
            req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          }
          for (var key in options.headers) {
            req.setRequestHeader(key, options.headers[key]);
          }
          if (options.progress) {
            req.onprogress=handleProgress;
          }
          req.onreadystatechange = function hdl(){
            if(req.readyState==4) {
              running = false;
              if (options.progress) {
                options.progress({
                  percent: 100
                });
              }
              if((/^[20]/).test(req.status)) {
                handleResp();
              }
              if((/^[45]/).test(req.status)) {
                handleError();
              }
            }
          };
          running = true;
          req.send(data||params);
        },
        cancel = function() {
          if (!running) {
            return;
          }
          running = false;
          req.abort();
          req.onreadystatechange = function(){};
          promise.reject('cancel');
        };
      promise.then(function(data) {
        options.success&&options.success(data.data, data.response);
      }, 
      function(data) {
        if ((options.format||'text').toLowerCase()=='jsend') {
          return options[data.status] && options[data.status](data);
        }
        options.error&&options.error(data);
      });

      if (method=='get' && !(/\?/.test(url))) {
        url += '?';
      }

      promise.xhr = req;
      promise.send = send;
      promise.isRunning = function(){return isRunning;};
      promise.cancel = cancel;
      if (options.send) {
        setTimeout(function() {
          send(params);
        }, 0);
      }

      return promise;
    };

  return {
    get:function(url, opts) {
      opts = opts||{};
      opts.method = 'get';
      opts.send = opts.send||true;

      var rs = xhr(url, opts);
      return rs;
    },
    post:function(url, opts) {
      opts = opts||{};
      opts.method = 'post';

      opts.send = opts.send||true;
      var rs = xhr(url, opts);
      return rs;
    }
  };