define('Buri/storage',[], function(){
  "use strict";
  var
    id = 1,
    storage = {},
    StorageClass = function(obj) {
      this.id = obj._storageId = id++;
      storage[this.id] = {};
    };

  StorageClass.prototype = {
    constructor: StorageClass,
    get:function(key) {
      return storage[this.id][key] || false;
    },
    set:function(key, value) {
      storage[this.id][key] = value;
      return this;
    }
  };
  StorageClass.getStore = function(key) {
    if (typeof key == 'string' || typeof key == 'number') {
      return false;
    }
    if (!key.storage) {
      key.storage = new StorageClass(key);
    }
    return key.storage;
  };
  StorageClass.removeStore = function(key) {
    if (typeof key == 'string' || typeof key == 'number') {
      return false;
    }
    if (key.storage) {
      delete key.storage;
    }
    return true;
  };
  return StorageClass;
});