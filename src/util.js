define('Buri/util',[], function(){
  "use strict";
  var util = {
    /**
    * Converts any array-like object into an actual array.
    * @param {Any[]} any array-like object
    * @return {Array} An actual array.
    */
    toArray: function(items) {
      return [].slice.call(items);
    },
    uniqueId : (function() {
      var idCounter = 0;
      return function(prefix) {
        var id = ++idCounter + '';
        return prefix ? prefix + id : id;
      };
    })(),
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    debounce : function(func, wait, immediate) {
      var timeout;
      return function() {
        var
          context = this,
          args = arguments,
          later = function() {
            timeout = null;
            if (!immediate) {
              func.apply(context, args);
            }
          },
          callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
      };
    },
    toQueryString:function(node, prefix){
      var
        queryString = [],
        key;
      for (key in node) {
        var
          k = prefix ? prefix + "[" + key + "]" : key,
          v = node[key];

        queryString.push(typeof v == "object" && v !== null ?
            util.toQueryString(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
      return queryString.join('&');
    },
    bind: function(method, thisValue) {
      return function() {
        return method.apply(thisValue, arguments);
      };
    },

    /**
     * Basic string formatting using %s and %d.
     * @param {String} text The text to format.
     * @param {Any} value* The values to replace in the text.
     * @return {String} The resulting string.
     */
    format: function(text) {
      var
        args = arguments,
        i = 1;
      return text.replace(/%[sd]/g, function() {
        return args[i++];
      });
    },
    template: function(html, object) {
      return html.replace(/\\?\{([^{}]+)\}/g, function(match, name){
        if (match.charAt(0) == '\\') {
          return match.slice(1);
        }
        return (object[name] !== null) ? object[name] : '';
      });
    },

    stringToElement: (function() {
      var
        wrapMap = {
          option: [1, '<select multiple="multiple">', '</select>'],
          legend: [1, '<fieldset>', '</fieldset>'],
          area: [1, '<map>', '</map>'],
          param: [1, '<object>', '</object>'],
          thead: [1, '<table>', '</table>'],
          tr: [2, '<table><tbody>', '</tbody></table>'],
          col: [2, '<table><tbody></tbody><colgroup>', '</colgroup></table>'],
          td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
          _default: [1, '<div>', '</div>']
        };

      wrapMap.optgroup = wrapMap.option;
      wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
      wrapMap.th = wrapMap.td;

      return function(html, fragment) {
        var
          el = document.createElement('section'),
          match = /<\s*\w.*?>/g.exec(html);
       
        if(match != null) {
          var
            tag = match[0].match(/<(\w+)(\s.*)?>/)[1],
            map = wrapMap[tag] || wrapMap._default;

          el.innerHTML = map[1] + html + map[2];
          // Descend through wrappers to the right content
          var j = map[0];
          while(j--) {
            el = el.lastChild;
          }
        } else {
          // if only text is passed
          el.innerHTML = html;
          el = el.lastChild;
        }
        el.normalize();
        var els = el.childNodes[0];
        if (el.childNodes.length>1) {
          var tmp = [];
          util.toArray(el.childNodes).forEach(function(child){
            if (child.nodeType!==3) {
              tmp.push(child);
            }
          });
          els = tmp.length === 1?tmp[0]:tmp;
        }
        if (fragment && el.childNodes.length>1) {
          var frag = document.createDocumentFragment();
          util.toArray(els).forEach(function(child){frag.appendChild(child);});
          els = frag;
        }
        return els;
      };
    })(),

    /**
     * Parses a string in the format of "foo:bar; foo:bar"
     * into an object with key-value pairs.
     * @param {String} text
     * @return {Object} An object with key-value pairs.
     * @method parse
     */
    parseData: function(text){
      var
        info = {},
        parts = text.split(/\s*;\s*/g),
        i = 0,
        len = parts.length,
        subparts;

      while(i < len){
        subparts = parts[i].split(/\s*:\s*/g);
        if (!subparts[1]) {
          info[i] = subparts[0];
        } else {
          info[subparts[0]] = subparts[1];
        }
        i++;
      }
      return info;
    },
    extend: function(destination, source) {
      for (var property in source) {
        if (source[property] && source[property].constructor &&
            source[property].constructor === Object) {
          destination[property] = destination[property] || {};
          util.extend(destination[property], source[property]);
        } else if (!destination[property]){
          destination[property] = source[property];
        }
      }
      return destination;
    },
    stringEndsWith: function (str, searchString) {
      if (String.prototype.endsWith) {
        return str.endsWith(searchString);
      }
      return str.indexOf(searchString, str.length - searchString.length) !== -1;
    },
    hasOwn : function(obj, prop){
      return Object.prototype.hasOwnProperty.call(obj, prop);
    },
    filter : function(obj, callback, thisObj) {
      var output = {};
      util.forIn(obj, function(val, key, obj2) {
        if (util.hasOwn(obj, key)) {
          if (callback.call(thisObj, val, key, obj2)) {
            output[key] = val;
          }
        }
      });

      return output;
    },
    forIn : (function() {
      var 
        _hasDontEnumBug,
        _dontEnums;

      function checkDontEnum(){
        _dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ];

        _hasDontEnumBug = true;

        for (var key in {'toString': null}) {
          _hasDontEnumBug = false;
        }
      }

      /**
       * Similar to Array/forEach but works over object properties and fixes Don't
       * Enum bug on IE.
       * based on: http://whattheheadsaid.com/2010/10/a-safer-object-keys-compatibility-implementation
       */
      function forIn(obj, fn, thisObj){
        var key, i = 0;
        // no need to check if argument is a real object that way we can use
        // it for arrays, functions, date, etc.

        //post-pone check till needed
        if (_hasDontEnumBug === null) {
          checkDontEnum();
        }

        for (key in obj) {
          if (exec(fn, obj, key, thisObj) === false) {
            break;
          }
        }

        if (_hasDontEnumBug) {
          while ((key = _dontEnums[i++])) {
            // since we aren't using hasOwn check we need to make sure the
            // property was overwritten
            if (obj[key] !== Object.prototype[key]) {

              if (exec(fn, obj, key, thisObj) === false) {
                break;
              }
            }
          }
        }
      }
      function exec(fn, obj, key, thisObj){
        return fn.call(thisObj, obj[key], key, obj);
      }
      return forIn;
    })(),
    hashCode: function(str){
      var 
        hash = 0, 
        length = str.length,
        i, Char;
      if (length === 0) {
        return hash;
      }
      /* jshint -W016 */
      for (i = 0; i < length; i++) {
        Char  = str.charCodeAt(i);
        hash  = ((hash<<5)-hash)+Char;
        hash |= 0; // Convert to 32bit integer
      }
      /* jshint +W016 */
      return hash;
    },
    isInt: (function(){
      var intRegex = /^\d+$/;
      return function(n) {
        return intRegex.test(n);
      };
    })(),
    objectGet: (function() {
      var getIndex = function(obj,i) {
        return obj?obj[i]:false;
      };
      return function(key, obj) {
        obj = obj||window;
        return (key||"").split('.').reduce(getIndex, obj);
      };
    })(),
    uniqueValues: function(array) {
      var u = {}, a = [];
      for(var i = 0, l = array.length; i < l; ++i){
        if(util.hasOwn(u, array[i])) {
          continue;
        }
        a.push(array[i]);
        u[array[i]] = 1;
      }
      return a;
    }
  };
  return util;
});