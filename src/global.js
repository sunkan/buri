if (window.buri_global) {
  define('Buri/global', [
    'Buri/util',
    'Buri/dom',
    'Buri/event',
    'Buri/class',
    'Buri/jsonp',
    'Buri/xhr',
    'Buri/promise',
    'Buri/storage'
  ], function(util, dom, event, klass, jsonp, xhr, promise, storage){
    if (typeof Buri === 'undefined') {
      window.Buri = {};
    }

    var makeGlobal = function(ns, obj) {
      if (typeof Buri[ns] === 'undefined') {
        Buri[ns] = obj;
      }
    }
    makeGlobal('util', util);
    makeGlobal('dom', dom);
    makeGlobal('event', event);
    makeGlobal('jsonp', jsonp);
    makeGlobal('xhr', xhr);
    makeGlobal('storage', storage);
    makeGlobal('Promise', promise);
  });
}