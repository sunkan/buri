define('Buri/dom',['Buri/util'], function(util){
  "use strict";
  var 
    dom = {},
    doc = document,
    isReady = false,
    isBound = false,
    readyClbs = [],
    doScrollCheck = function() {
      if ( isReady ) {
        return;
      }

      try {
        // If IE is used, use the trick by Diego Perini
        // http://javascript.nwbox.com/IEContentLoaded/
        document.documentElement.doScroll("left");
      } catch(e) {
        setTimeout( doScrollCheck, 1 );
        return;
      }
      // and execute any waiting functions
      dom.ready();
    },
    DOMContentLoaded = (function() {
      // Cleanup functions for the document ready method
      if ( document.addEventListener ) {
        return function() {
          document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
          dom.ready();
        };
      } else if ( document.attachEvent ) {
        DOMContentLoaded = function() {
          // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
          if ( document.readyState === "complete" ) {
            document.detachEvent( "onreadystatechange", DOMContentLoaded );
            dom.ready();
          }
        };
      }
    })(),
    bindReady = function(){
      if (isBound) {
        return ;
      }
      isBound = true;
      if ( document.readyState === "complete" ) {
        // Handle it asynchronously to allow scripts the opportunity to delay ready
        return setTimeout( dom.ready, 1 );
      }

      if ( document.addEventListener ) {
        document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );

        // A fallback to window.onload, that will always work
        window.addEventListener( "load", dom.ready, false );
      // If IE event model is used
      } else if ( document.attachEvent ) {
        // ensure firing before onload,
        // maybe late but safe also for iframes
        document.attachEvent( "onreadystatechange", DOMContentLoaded );

        // A fallback to window.onload, that will always work
        window.attachEvent( "onload", dom.ready );

        // If IE and not a frame
        // continually check to see if the document is ready
        var toplevel = false;

        try {
          toplevel = window.frameElement === null;
        } catch(e) {}

        if ( document.documentElement.doScroll && toplevel ) {
          doScrollCheck();
        }
      }
    },
    match = (function(el) {
      if (!el) {
        return;
      }
      var p = el.prototype;
      return (p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector);
    }(typeof HTMLElement !== "undefined"? HTMLElement : Element)),
    inserters = {
      before: function(into, element){
        var parent = into.parentNode;
        if (parent) {
          parent.insertBefore(element, into);
        }
      },
      after: function(into, element){
        var parent = into.parentNode;
        if (parent) {
          parent.insertBefore(element, into.nextSibling);
        }
      },
      bottom: function(into, element){
        into.appendChild(element);
      },
      top: function(into, element){
        into.insertBefore(element, into.firstChild);
      }
    },
    replList = {
      html:'innerHTML',
      text:(document.createElement('div').textContent === null) ? 'innerText': 'textContent'
    };

  dom.ready = function(clb) {
    bindReady();
    if (typeof(clb) == 'function') {
      readyClbs.push(clb);
      if (isReady) {
        clb();
      }
      return;
    }
    // Either a released hold or an DOMready/load event and not yet ready
    if ( !isReady ) {
      // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
      if ( !document.body ) {
        return setTimeout( dom.ready, 1 );
      }
      // Remember that the DOM is ready
      isReady = true;

      // If there are functions bound, to execute
      readyClbs.forEach(function(clb) {
        clb();
      });
    }
  };


  dom.select = function(selector, node) {
    if (!node && /^#[a-z0-9\-_]+$/i.test(selector)) {
      return document.getElementById(selector.slice(1));
    }
    return (node||document).querySelector(selector);
  };

  dom.selectAll = function(selector, node){
    var nodes;
    if (/^[a-z]+$/i.test(selector)) {
      nodes = (node||document).getElementsByTagName(selector);
    } else if (!node && /^#[a-z0-9\-_]+$/i.test(selector)) {
      nodes = document.getElementById(selector.slice(1));
    } else {
      nodes = (node||document).querySelectorAll(selector);
    }
    return util.toArray(nodes);
  };
  dom.contains = function(parent, node) {
    return parent.contains(node);
  };
  dom.match = function(node, selector) {
    if (!selector) {
      return true;
    }

    if (/^[a-z]+$/i.test(selector)) {
      return selector.toUpperCase()===node.tagName;
    } else if (/^#[a-z0-9\-_]+$/i.test(selector)) {
      return selector.slice(1)===node.id;
    }
    return match.call(node, selector);
  };
  dom.inject = function(into, node, pos) {
    if (node.length) {
      var fragment = document.createDocumentFragment();
      for (var i = node.length - 1; i >= 0; i--) {
        fragment.appendChild(node[i]);
      }
      node = fragment;
    }
    inserters[pos||'bottom'](into, node);
  };
  dom.setStyle = function(node, name, value) {
    if (typeof(name) === 'string') {
      node.style[name] = value;
    } else {
      for (var key in name) {
        dom.setStyle(node, key, name[key]);
      }
    }
  };
  dom.getStyle = function(node, style) {
    if(document.defaultView && document.defaultView.getComputedStyle){
      return document.defaultView.getComputedStyle(node, "").getPropertyValue(style.replace(/[A-Z]/g, function(match){
        return ('-' + match.charAt(0).toLowerCase());
      }));
    }
    return false;
  };
  dom.get = function(node, attr) {
    if (replList[attr]) {
      return node[replList[attr]];
    } else {
      return node.getAttribute(attr);
    }
  };
  dom.set = function(node, attr, value) {
    if (replList[attr]) {
      node[replList[attr]] = value;
    } else {
      if (value === false) {
        node.removeAttribute(attr);
      } else {
        node.setAttribute(attr, value);
      }
    }
    return node;
  };
  dom.addClass = function(nodes, className) {
    nodes = (nodes.length ? nodes : [nodes]);
    for (var i=0, len=nodes.length; i < len; i++) {
      nodes[i].classList.add(className);
    }
  };
  dom.hasClass = function(node, className){
    return node.classList.contains(className);
  };
  dom.removeClass = function(nodes, className){
    nodes = (nodes.length ? nodes : [nodes]);
    for (var i=0, len=nodes.length; i < len; i++){
      nodes[i].classList.remove(className);
    }
  };

  dom.getNext = function(node, selector) {
    for ( ; node; node = node.nextSibling ) {
      if ( node.nodeType == 1 && dom.match(node, selector)) {
        return node;
      }
    }
    return false;
  };

  dom.getChildren = function(node, selector, skipMe) {
    var nodes = [];
    node = node.firstChild;
    for ( ; node; node = node.nextSibling ) {
      if ( node.nodeType == 1 && node != skipMe) {
        if (
          (typeof selector === 'function' && selector(el)) ||
          (dom.match(node, selector))
        ) {
          nodes.push( node );
        }
      }
    }
    return nodes;
  };
  dom.getSiblings = function(node, selector) {
    return dom.getChildren(node.parentNode, selector, node);
  };
  dom.ancestor = function(node, condition){
    var parentNode = node;
    while(parentNode && parentNode.nodeType == 1 && parentNode != document.body){
      if ((typeof condition == 'string' && dom.match(parentNode, condition)) || (typeof condition == 'function' && condition(parentNode))) {
        return parentNode;
      }
      parentNode = parentNode.parentNode;
    }
    return null;
  };
  dom.ancestorByTag = function(node, tag){
    return dom.ancestor(node, function(node){
      return node.tagName == tag.toUpperCase();
    });
  };
  dom.ancestorByClass = function(node, className){
    return dom.ancestor(node, function(node){
      return dom.hasClass(node, className);
    });
  };
  dom.ancestorByAttribute = function(node, attribute){
    return dom.ancestor(node, function(node){
      return node.getAttribute(attribute) !== null;
    });
  };

  var toQueryString = util.toQueryString;

  util.toQueryString = function(node, prefix) {
    if (node.nodeType) {
      var queryString = [];
      dom.selectAll('input, select, textarea', node).forEach( function(el) {
        var type = el.type.toLowerCase(), value;
        if (el.disabled || type == 'submit' || type == 'reset' || type == 'file' || type == 'image') {
          return;
        }
        value = (el.tagName == 'SELECT') ? (function() {
          var r = [], i;
          for (i = el.options.length - 1; i >= 0; i--) {
            el.options[i].selected && r.push(el.options[i].value);
          }
          return r;
        }()) : ((type == 'radio' || type == 'checkbox') && !el.checked) ? null : el.value;

        (typeof value == 'string' ? [value] : util.toArray()).forEach(function(val){
          if (typeof val != 'undefined') {
            queryString.push(encodeURIComponent(el.name) + '=' + encodeURIComponent(val));
          }
        });
      });
      return queryString.join('&'); 
    }
    return toQueryString(node);
  };
  util.one2many = function(tpl, inCount) {
    var nodes;
    if (tpl.getAttribute('data-many')) {
      nodes = [tpl];
    } else {
      nodes = dom.selectAll('[data-many]', tpl);
    }
    if (!nodes.length) {
      return tpl;
    }
    nodes.forEach(function(node){
      var count = inCount || parseInt(node.parentNode.innerHTML, 10);
      if (count>1) {
        node.innerHTML = util.format(node.getAttribute('data-many'), count);
      } else if (count == 1 && node.getAttribute('data-one')) {
        node.innerHTML = util.format(node.getAttribute('data-one'), count);
      }
    });
    return tpl;
  };

  return dom;
});