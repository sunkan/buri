// =============================================================================
// Project:        Include.js - Javascript loader
// Copyright:      ©2011-2013 Jérémy Barbe (http://shwaark.com) and contributors
// Licence:        Licence under MIT license
// =============================================================================

(function (environment) {
  "use strict";

  /**
   * List a existings modules
   * @type {Object}
   */
  var modules = {};

  /**
   * Array of waiting modules
   * @type {Array}
   */
  var waitingModules = [];

  /**
   * Count created script for control
   * @type {Number}
   */
  var scriptCounter = 1;

  /**
   * Base element check for IE 6-8
   * @type {Node}
   */
  var baseElement = document.getElementsByTagName('base')[0];

  /**
   * Head element
   * @type {Node}
   */
  var head = document.getElementsByTagName('head')[0];

  /**
   * Loop trougth an array of element with the given function
   * @param  {Array|NodeList}    array    array to loop       
   * @param  {Function} callback function to execute with each element
   */
  function each(array, callback) {
    var i;

    for (i = 0; i < array.length; i++) {
      if (array[i] !== undefined && callback(array[i], i, array) === false) {
        break;
      }
    }
  }

  /**
   * Check if a module is loaded
   */
  function checkModuleLoaded() {
    each(waitingModules, function (module, i) {
      var 
        name         = module[0],
        dependencies = module[1],
        exec         = module[2],
        args         = [];

      each(dependencies, function (dependencie, n) {
        n = dependencie.push ? dependencie[0] : dependencie;

        if (modules[n] !== undefined) {
          //console.log(n, modules[n]);
          args.push(modules[n]);
        }
      });

      if (dependencies.length === args.length || dependencies.length === 0) {
        exec = typeof exec == 'function' ? exec.apply(this, args) : exec;
        module.push(true);

        if (name !== null) {
          modules[name] = exec;
        }

        if (name === null && i+1 === waitingModules.length) {
          waitingModules = [];
          scriptCounter = 1;
        }
      }
    });
  }

  /**
   * On Load event
   * @param  {Event}      event  event of the load
   */
  function onLoad(event) {
    var 
      target = (event.currentTarget || event.srcElement),
      name,
      count;

    //Check if the script is realy loaded and executed ! (Fuck you IE with your "Loaded but not realy, wait to be completed")
    if (event.type !== "load" && target.readyState != "complete") {
      return;
    }

    name = target.getAttribute('data-module');
    count = target.getAttribute('data-count');
    target.setAttribute('data-loaded', true);

    // Old browser need to use the detachEvent method
    if (target.attachEvent) {
      target.detachEvent('onreadystatechange', onLoad);
    } else {
      target.removeEventListener('load', onLoad);
    }

    // Is this script add a waiting module ? If not, that's a "normal" script file
    if (count > waitingModules.length) {
      scriptCounter--;
      modules[name] = modules[name] || scriptCounter;
    } else if (waitingModules[0][0] === null) {
      waitingModules[0][0] = name;
    }

    checkModuleLoaded();
  }

  /**
   * Attach events to a script tags
   * @param {Element} script     script to attach event
   */
  function attachEvents(script) {
    if (script.attachEvent) {
      script.attachEvent('onreadystatechange', onLoad);
    } else {
      script.addEventListener('load', onLoad, false);
    }
  }

  /**
   * Check if a script already load
   * @param  {String} moduleName module to load
   */
  function checkScripts(moduleName) {
    var script = false;

    each(document.getElementsByTagName('script'), function (elem) {
      if (elem.getAttribute('data-module') && elem.getAttribute('data-module') === moduleName) {
        script = elem;
        return false;
      }
    });

    return script;
  }

  /**
   * Create a script element to load asked module
   * @param  {String} moduleName name of the module
   * @param  {String} moduleFile fiel to include
   */
  function create(moduleName, moduleFile) {
    //SetTimeout prevent the "OMG RUN, CREATE THE SCRIPT ELEMENT, YOU FOOL" browser rush
    setTimeout(function(){
      var script = checkScripts(moduleName);
      if (script) {
        return;
      }

      scriptCounter++;

      script = document.createElement('script');

      script.async = true;
      script.type = "text/javascript";
      script.src = 'http://127.0.0.1/' + moduleFile;
      script.setAttribute('data-module', moduleName);
      script.setAttribute('data-count',  scriptCounter);
      script.setAttribute('data-loaded', false);
      console.log(script);
      if (baseElement) {
        //prevent IE 6-8 bug (script executed before appenchild execution. Yeah, that's realy SUCK)
        baseElement.parentNode.insertBefore(script, baseElement);
      } else {
        head.appendChild(script);
      }

      attachEvents(script);
    }, 0);
  }

  /**
   * Parse a file to include
   * @param  {String} file  file to parse
   */
  function parseFiles(file) {
    var moduleName = file.push ? file[0] : file;
    var moduleFile = file.push ? file[1] : file;

    //Don't load module already loaded
    if (modules[moduleName]) {
      checkModuleLoaded();
      return;
    }
    console.log(moduleName, modules[moduleName], modules);
      

    if (!/\.js/.test(moduleFile) && !/^http/.test(moduleFile)) {
      moduleFile = moduleFile.replace('.', '/');
      moduleFile = moduleFile + '.js';
    }
    console.log('load file', moduleFile);
    create(moduleName, moduleFile);
  }

  /**
   * @param {String}   name     the name of the module
   * @param {Array}    deps     dependencies of the module
   * @param {Function} module   module definition
   */
  environment.include = environment.require = environment.define = function (name, deps, module) {
    if (typeof name !== "string") {
      module = deps;
      deps = name;
      name = null;
    }

    if (deps.constructor !== [].constructor) {
      module = deps;
      deps = [];
    }

    waitingModules.unshift([name, deps, module]);

    checkModuleLoaded();

    if (deps.length) {
      each(deps, parseFiles);
    }
  };
  environment._m = modules;

})(this);
define('Buri/dev',[], function(){
  "use strict";
  var 
    history = [],
    con = window.console||false;

  return {
    log : function(){
      history.push(arguments);
      if (con) {
        con.log[ con.firebug ? 'apply' : 'call']( con, Array.prototype.slice.call( arguments ) );
      }
    },
    history : function() {
      return history;
    }
  };
});

define('Buri/util',[], function(){
  "use strict";
  var util = {
    /**
    * Converts any array-like object into an actual array.
    * @param {Any[]} any array-like object
    * @return {Array} An actual array.
    */
    toArray: function(items) {
      return [].slice.call(items);
    },
    uniqueId : (function() {
      var idCounter = 0;
      return function(prefix) {
        var id = ++idCounter + '';
        return prefix ? prefix + id : id;
      };
    })(),
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    debounce : function(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) {
            func.apply(context, args);
          }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
      };
    },
    toQueryString:function(node, prefix){
      var queryString = [], key;
      for (key in node) {
        var 
          k = prefix ? prefix + "[" + key + "]" : key,
          v = node[key];

        queryString.push(typeof v == "object" && v !== null ?
            util.toQueryString(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
      return queryString.join('&');
    },
    bind: function(method, thisValue) {
      return function() {
        return method.apply(thisValue, arguments);
      };
    },

    /**
     * Basic string formatting using %s and %d.
     * @param {String} text The text to format.
     * @param {Any} value* The values to replace in the text.
     * @return {String} The resulting string.
     */
    format: function(text) {
      var args = arguments,
        i = 1;
      return text.replace(/%[sd]/g, function() {
        return args[i++];
      });
    },
    template: function(html, object) {
      return html.replace(/\\?\{([^{}]+)\}/g, function(match, name){
        if (match.charAt(0) == '\\') {
          return match.slice(1);
        }
        return (object[name] !== null) ? object[name] : '';
      });
    },
    stringToElement: (function() {
      var el = document.createElement('section');
      return function(html, fragment) {
        el.innerHTML = html;
        var els = el.childNodes.length == 1?el.childNodes[0]:util.toArray(el.childNodes);
        if (fragment && el.childNodes.length>1) {
          var frag = document.createDocumentFragment();
          els.forEach(function(child){frag.appendChild(child);});
          els = frag;
        }
        return els;
      };
    })(),

    /**
     * Parses a string in the format of "foo:bar; foo:bar"
     * into an object with key-value pairs.
     * @param {String} text
     * @return {Object} An object with key-value pairs.
     * @method parse
     */
    parseData: function(text){
      var info = {},
        parts = text.split(/\s*;\s*/g),
        i = 0,
        len = parts.length,
        subparts;

      while(i < len){
        subparts = parts[i].split(/\s*:\s*/g);
        if (!subparts[1]) {
          info[i] = subparts[0];
        } else {
          info[subparts[0]] = subparts[1];
        }
        i++;
      }
      return info;
    },
    extend: function(destination, source) {
      for (var property in source) {
        if (source[property] && source[property].constructor &&
            source[property].constructor === Object) {
          destination[property] = destination[property] || {};
          util.extend(destination[property], source[property]);
        } else if (!destination[property]){
          destination[property] = source[property];
        }
      }
      return destination;
    },
    stringEndsWith: function (str, suffix) {
      if (!str) {
        return false;
      }
      return str.indexOf(suffix, str.length - suffix.length) !== -1;
    },
    hasOwn : function(obj, prop){
      return Object.prototype.hasOwnProperty.call(obj, prop);
    },
    filter : function(obj, callback, thisObj) {
      var output = {};
      util.forIn(obj, function(val, key, obj2) {
        if (util.hasOwn(obj, key)) {
          if (callback.call(thisObj, val, key, obj2)) {
            output[key] = val;
          }
        }
      });

      return output;
    },
    forIn : (function() {
      var 
        _hasDontEnumBug,
        _dontEnums;

      function checkDontEnum(){
        _dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ];

        _hasDontEnumBug = true;

        for (var key in {'toString': null}) {
          _hasDontEnumBug = false;
        }
      }

      /**
       * Similar to Array/forEach but works over object properties and fixes Don't
       * Enum bug on IE.
       * based on: http://whattheheadsaid.com/2010/10/a-safer-object-keys-compatibility-implementation
       */
      function forIn(obj, fn, thisObj){
        var key, i = 0;
        // no need to check if argument is a real object that way we can use
        // it for arrays, functions, date, etc.

        //post-pone check till needed
        if (_hasDontEnumBug === null) {
          checkDontEnum();
        }

        for (key in obj) {
          if (exec(fn, obj, key, thisObj) === false) {
            break;
          }
        }

        if (_hasDontEnumBug) {
          while ((key = _dontEnums[i++])) {
            // since we aren't using hasOwn check we need to make sure the
            // property was overwritten
            if (obj[key] !== Object.prototype[key]) {
              if (exec(fn, obj, key, thisObj) === false) {
                break;
              }
            }
          }
        }
      }
      function exec(fn, obj, key, thisObj){
        return fn.call(thisObj, obj[key], key, obj);
      }
      return forIn;
    })(),
    hashCode: function(str){
      var 
        hash = 0, 
        length = str.length,
        i, Char;
      if (length === 0) {
        return hash;
      }
      /* jshint -W016 */
      for (i = 0; i < length; i++) {
        Char  = str.charCodeAt(i);
        hash  = ((hash<<5)-hash)+Char;
        hash |= 0; // Convert to 32bit integer
      }
      /* jshint +W016 */
      return hash;
    },
    isInt: (function(){
      var intRegex = /^\d+$/;
      return function(n) {
        return intRegex.test(n);
      };
    })(),
    objectGet: (function() {
      var getIndex = function(obj,i) {
        return obj?obj[i]:false;
      };
      return function(key, obj) {
        obj = obj||window;
        return key.split('.').reduce(getIndex, obj);
      };
    })()
  };
  return util;
});

define('Buri/class', ['Buri/util'], function(util){
  "use strict";
  var 
    defineProperty = Object.defineProperty,
    getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

  try {
    defineProperty({}, "~", {});
    getOwnPropertyDescriptor({}, "~");
  } catch (e){
    defineProperty = null;
    getOwnPropertyDescriptor = null;
  }

  var define = function(value, key, from){
    defineProperty(this, key, getOwnPropertyDescriptor(from, key) || {
      writable: true,
      enumerable: true,
      configurable: true,
      value: value
    });
  };
  var implement = function(proto){
    util.forIn(proto, defineProperty ? define : copyProp, this.prototype);
    return this;
  };
  var 
    mixIn = function(target, objects){
      var 
        i = 0,
        n = arguments.length,
        obj;
      while(++i < n){
        obj = arguments[i];
        if (obj !== null) {
          util.forIn(obj, function(val, key) {
            if (util.hasOwn(obj, key)) {
              return copyProp.call(target, obj[key], key, obj);
            }
          });
        }
      }
      return target;
    },
    copyProp = function(val, key){
      this[key] = val;
    },
    create = function(parent, props) {
      function F(){}
      F.prototype = parent;
      return mixIn(new F(), props);
    },
    verbs = /^constructor|inherits|mixin$/,
    klass = function(proto) {
      var superklass = proto.inherits;
      var constructor = (util.hasOwn(proto, "constructor")) ? proto.constructor : (superklass) ? function(){
        return superklass.apply(this, arguments);
      } : function(){};

      if (superklass){
        mixIn(constructor, superklass);

        var superproto = superklass.prototype;
        // inherit from superprime
        var cproto = constructor.prototype = create(superproto);

        // setting constructor.parent to superprime.prototype
        // because it's the shortest possible absolute reference
        constructor.parent = superproto;
        cproto.constructor = constructor;
      }

      if (!constructor.implement) {
        constructor.implement = implement;
      }

      // implement proto and return constructor
      return constructor.implement(util.filter(proto, function(value, key){
        return !key.match(verbs);
      }));
    };
  return klass;
});
define('Buri/storage',[], function(){
  "use strict";
  var
    id = 1,
    storage = {},
    StorageClass = function(obj) {
      this.id = obj._storageId = id++;
      storage[this.id] = {};
    };

  StorageClass.prototype = {
    constructor: StorageClass,
    get:function(key) {
      return storage[this.id][key] || false;
    },
    set:function(key, value) {
      storage[this.id][key] = value;
      return this;
    }
  };
  StorageClass.getStore = function(key) {
    if (typeof key == 'string' || typeof key == 'number') {
      return false;
    }
    if (!key.storage) {
      key.storage = new StorageClass(key);
    }
    return key.storage;
  };
  return StorageClass;
});
define('Buri/promise',[], function(){
  "use strict";
  return function() {
    var callbacks = [],
      resolve = function (result) {
        complete('resolve', result);
      },
      reject = function(err) {
        complete('reject', err);
      },
      then = function(resolve, reject) {
        callbacks.push({ resolve: resolve, reject: reject });
      },
      promise = {
        resolve: resolve,
        reject: reject,
        then: then
      };
     
    function complete(type, result) {
      promise.then = type === 'reject' ? function(resolve, reject) { 
        reject && reject(result); 
      } : function(resolve) { 
        resolve && resolve(result); 
      };
      promise.resolve = promise.reject = function() { throw new Error("Promise already completed"); };
      var i = 0, cb;
      /* jshint -W084 */
      while(cb = callbacks[i++]) { 
        cb[type] && cb[type](result); 
      }
      callbacks = null;
    }
     
    return promise;
  };
});
define('Buri/dom',['Buri/util'], function(util){
  "use strict";
  var 
    dom = {},
    doc = document,
    isReady = false,
    isBound = false,
    readyClbs = [],
    doScrollCheck = function() {
      if ( isReady ) {
        return;
      }

      try {
        // If IE is used, use the trick by Diego Perini
        // http://javascript.nwbox.com/IEContentLoaded/
        document.documentElement.doScroll("left");
      } catch(e) {
        setTimeout( doScrollCheck, 1 );
        return;
      }
      // and execute any waiting functions
      dom.ready();
    },
    DOMContentLoaded = (function() {
      // Cleanup functions for the document ready method
      if ( document.addEventListener ) {
        return function() {
          document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
          dom.ready();
        };
      } else if ( document.attachEvent ) {
        DOMContentLoaded = function() {
          // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
          if ( document.readyState === "complete" ) {
            document.detachEvent( "onreadystatechange", DOMContentLoaded );
            dom.ready();
          }
        };
      }
    })(),
    bindReady = function(){
      if (isBound) {
        return ;
      }
      isBound = true;
      if ( document.readyState === "complete" ) {
        // Handle it asynchronously to allow scripts the opportunity to delay ready
        return setTimeout( dom.ready, 1 );
      }

      if ( document.addEventListener ) {
        document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );

        // A fallback to window.onload, that will always work
        window.addEventListener( "load", dom.ready, false );
      // If IE event model is used
      } else if ( document.attachEvent ) {
        // ensure firing before onload,
        // maybe late but safe also for iframes
        document.attachEvent( "onreadystatechange", DOMContentLoaded );

        // A fallback to window.onload, that will always work
        window.attachEvent( "onload", dom.ready );

        // If IE and not a frame
        // continually check to see if the document is ready
        var toplevel = false;

        try {
          toplevel = window.frameElement === null;
        } catch(e) {}

        if ( document.documentElement.doScroll && toplevel ) {
          doScrollCheck();
        }
      }
    },
    match = (function(el) {
      if (!el) {
        return;
      }
      var p = el.prototype;
      return (p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector);
    }(typeof HTMLElement !== "undefined"? HTMLElement : Element)),
    inserters = {
      before: function(into, element){
        var parent = into.parentNode;
        if (parent) {
          parent.insertBefore(element, into);
        }
      },
      after: function(into, element){
        var parent = into.parentNode;
        if (parent) {
          parent.insertBefore(element, into.nextSibling);
        }
      },
      bottom: function(into, element){
        into.appendChild(element);
      },
      top: function(into, element){
        into.insertBefore(element, into.firstChild);
      }
    },
    replList = {
      html:'innerHTML',
      text:(document.createElement('div').textContent === null) ? 'innerText': 'textContent'
    };

  dom.ready = function(clb) {
    bindReady();
    if (typeof(clb) == 'function') {
      readyClbs.push(clb);
      if (isReady) {
        clb();
      }
      return;
    }
    // Either a released hold or an DOMready/load event and not yet ready
    if ( !isReady ) {
      // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
      if ( !document.body ) {
        return setTimeout( dom.ready, 1 );
      }
      // Remember that the DOM is ready
      isReady = true;

      // If there are functions bound, to execute
      readyClbs.forEach(function(clb) {
        clb();
      });
    }
  };


  dom.select = function(selector, node) {
    if (!node && /^#[a-z0-9\-_]+$/i.test(selector)) {
      return document.getElementById(selector.slice(1));
    }
    return (node||document).querySelector(selector);
  };

  dom.selectAll = function(selector, node){
    var nodes;
    if (/^[a-z]+$/i.test(selector)) {
      nodes = (node||document).getElementsByTagName(selector);
    } else if (!node && /^#[a-z0-9\-_]+$/i.test(selector)) {
      nodes = document.getElementById(selector.slice(1));
    } else {
      nodes = (node||document).querySelectorAll(selector);
    }
    return util.toArray(nodes);
  };
  dom.contains = function(parent, node) {
    return parent.contains(node);
  };
  dom.match = function(node, selector) {
    if (!selector) {
      return true;
    }

    if (/^[a-z]+$/i.test(selector)) {
      return selector.toUpperCase()===node.tagName;
    } else if (/^#[a-z0-9\-_]+$/i.test(selector)) {
      return selector.slice(1)===node.id;
    }
    return match.call(node, selector);
  };
  dom.inject = function(into, node, pos) {
    if (node.length) {
      var fragment = document.createDocumentFragment();
      for (var i = node.length - 1; i >= 0; i--) {
        fragment.appendChild(node[i]);
      }
      node = fragment;
    }
    inserters[pos||'bottom'](into, node);
  };
  dom.setStyle = function(node, name, value) {
    if (typeof(name) === 'string') {
      node.style[name] = value;
    } else {
      for (var key in name) {
        dom.setStyle(node, key, name[key]);
      }
    }
  };
  dom.getStyle = function(node, style) {
    if(document.defaultView && document.defaultView.getComputedStyle){
      return document.defaultView.getComputedStyle(node, "").getPropertyValue(style.replace(/[A-Z]/g, function(match){
        return ('-' + match.charAt(0).toLowerCase());
      }));
    }
    return false;
  };
  dom.get = function(node, attr) {
    if (replList[attr]) {
      return node[replList[attr]];
    } else {
      return node.getAttribute(attr);
    }
  };
  dom.set = function(node, attr, value) {
    if (replList[attr]) {
      node[replList[attr]] = value;
    } else {
      if (value === false) {
        node.removeAttribute(attr);
      } else {
        node.setAttribute(attr, value);
      }
    }
    return node;
  };
  dom.addClass = function(nodes, className) {
    nodes = (nodes.length ? nodes : [nodes]);
    for (var i=0, len=nodes.length; i < len; i++) {
      nodes[i].classList.add(className);
    }
  };
  dom.hasClass = function(node, className){
    return node.classList.contains(className);
  };
  dom.removeClass = function(nodes, className){
    nodes = (nodes.length ? nodes : [nodes]);
    for (var i=0, len=nodes.length; i < len; i++){
      nodes[i].classList.remove(className);
    }
  };

  dom.getNext = function(node, selector) {
    for ( ; node; node = node.nextSibling ) {
      if ( node.nodeType == 1 && dom.match(node, selector)) {
        return node;
      }
    }
    return false;
  };

  dom.getChildren = function(node, selector, skipMe) {
    var nodes = [];
    node = node.firstChild;
    for ( ; node; node = node.nextSibling ) {
      if ( node.nodeType == 1 && node != skipMe) {
        if (
          (typeof selector === 'function' && selector(el)) ||
          (dom.match(node, selector))
        ) {
          nodes.push( node );
        }
      }
    }
    return nodes;
  };
  dom.getSiblings = function(node, selector) {
    return dom.getChildren(node.parentNode, selector, node);
  };
  dom.ancestor = function(node, condition){
    var parentNode = node;
    while(parentNode && parentNode.nodeType == 1 && parentNode != document.body){
      if ((typeof condition == 'string' && dom.match(parentNode, condition)) || (typeof condition == 'function' && condition(parentNode))) {
        return parentNode;
      }
      parentNode = parentNode.parentNode;
    }
    return null;
  };
  dom.ancestorByTag = function(node, tag){
    return dom.ancestor(node, function(node){
      return node.tagName == tag.toUpperCase();
    });
  };
  dom.ancestorByClass = function(node, className){
    return dom.ancestor(node, function(node){
      return dom.hasClass(node, className);
    });
  };
  dom.ancestorByAttribute = function(node, attribute){
    return dom.ancestor(node, function(node){
      return node.getAttribute(attribute) !== null;
    });
  };

  var toQueryString = util.toQueryString;

  util.toQueryString = function(node, prefix) {
    if (node.nodeType) {
      var queryString = [];
      dom.selectAll('input, select, textarea', node).forEach( function(el) {
        var type = el.type.toLowerCase(), value;
        if (el.disabled || type == 'submit' || type == 'reset' || type == 'file' || type == 'image') {
          return;
        }
        value = (el.tagName == 'SELECT') ? (function() {
          var r = [], i;
          for (i = el.options.length - 1; i >= 0; i--) {
            el.options[i].selected && r.push(el.options[i].value);
          }
          return r;
        }()) : ((type == 'radio' || type == 'checkbox') && !el.checked) ? null : el.value;

        (typeof value == 'string' ? [value] : util.toArray()).forEach(function(val){
          if (typeof val != 'undefined') {
            queryString.push(encodeURIComponent(el.name) + '=' + encodeURIComponent(val));
          }
        });
      });
      return queryString.join('&'); 
    }
    return toQueryString(node);
  };
  util.one2many = function(tpl, inCount) {
    var nodes;
    if (tpl.getAttribute('data-many')) {
      nodes = [tpl];
    } else {
      nodes = dom.selectAll('[data-many]', tpl);
    }
    if (!nodes.length) {
      return tpl;
    }
    nodes.forEach(function(node){
      var count = inCount || parseInt(node.parentNode.innerHTML, 10);
      if (count>1) {
        node.innerHTML = util.format(node.getAttribute('data-many'), count);
      } else if (count == 1 && node.getAttribute('data-one')) {
        node.innerHTML = util.format(node.getAttribute('data-one'), count);
      }
    });
    return tpl;
  };

  return dom;
});
define('Buri/event',['Buri/util','Buri/dom'], function(util, dom) {
  "use strict";
  var
    id=1,
    EVENTIGNORE = 'BuriDelegateIgnore',
    listeners = [],
    working,
    handleDelegate = function(e) {
      var
        root = this,
        listenerList = listeners[root.delegatId][e.type],
        l = listenerList.length,
        target = e.target,
        listener,
        returned,
        i;

      if (e[EVENTIGNORE] === true) {
        return;
      }
      while (target && l) {
        for (i = 0; i < l; i++) {
          listener = listenerList[i];

          if (!listener) {
            break;
          }

          if (dom.match(target, listener.selector)) {
            returned = listener.handler.call(target, e, target);
          }
          if (returned === false) {
            e[EVENTIGNORE] = true;
            return;
          }
        }

        if (target === root) {
          break;
        }
        l = listenerList.length;
        target = target.parentElement;
      }
    },
    addEvent = function(target, eventName, handler){
      if (eventName.indexOf(':relay')!==-1) {
        if (working) {
          setTimeout(function(){
            addEvent(target, eventName, handler);
          },10);
          return ;
        }
        working = true;
        eventName = eventName.split(':');
        if (!target.delegatId) {
          target.delegatId = id++;
          listeners[target.delegatId] = {};
        }
        if (!listeners[target.delegatId][eventName[0]]) {
          listeners[target.delegatId][eventName[0]] = [];
          if (target.addEventListener) {
            target.addEventListener(eventName[0], handleDelegate, false);
          } else {
            target.attachEvent(eventName[0], handleDelegate);
          }

        }
        listeners[target.delegatId][eventName[0]].push({
          selector: eventName[1].substr(6, eventName[1].length-7),
          handler : handler,
          event: eventName[0]
        });
        working = false;
      } else if (eventName.indexOf(':debounce')!==-1) {
        var parts = eventName.split(':');
        handler = util.debounce(handler, parseInt(eventName.match(/\(([0-9]+)\)/)[1]), true);
        addEvent(parts[0], eventName, handler);
      } else if (eventName.indexOf(':once')!==-1 ) {
        var onceHandler = function(e) {
          event.off(target, eventName.split(':')[0], onceHandler);
          handler.apply(this, [e]);
        };
        addEvent(target, eventName.split(':')[0], onceHandler);
      } else {
        if (target.addEventListener) {
          target.addEventListener(eventName, handler, false);
        } else {
          target.attachEvent(eventName, handler);
        }
      }
    },
    event = {
      on: addEvent,
      off: function(target, eventName, handler) {
        if (target.removeEventListener) {
          target.removeEventListener(eventName, handler, false);
        } else {
          target.detachEvent(eventName, handler);
        }
      },
      getTarget: function(e) {
        return e.target;
      },
      preventDefault: function(event){
        if (event.preventDefault){
          event.preventDefault();
        } else {
          event.returnValue = false;
        }
      },
      extendObject: function(obj) {
        var listeners = {};
        obj.trigger = function(event, args) {
          if (!listeners[event] || listeners[event].length === 0) {
            return ;
          }
          listeners[event].forEach(function(clb){
            setTimeout(function(){
              clb.apply(obj, args);
            }, 0);
          });
        };
        obj.on = function(event, clb) {
          if (!listeners[event]) {
            listeners[event] = [];
          }
          listeners[event].push(clb);
          return this;
        };
        return obj;
      }
    };

  return event;
});
define('Buri/xhr', ['Buri/util', 'Buri/promise'], function(util, Promise) {
  "use strict";
  var 
    xhr = function(url, options) {
      options = options || {};
      var
        promise = new Promise(),
        req    = new XMLHttpRequest(),
        method = options.method || 'get',
        async  = (typeof options.async != 'undefined' ? options.async : true),
        params = options.data || null,
        running = false,
        handleResp = function() {
          var 
            format = options.format||'text',
            response = req.responseText;

          if (format.toLowerCase()=='json') {
            promise.resolve(JSON.parse(response));
          } else if (format.toLowerCase()=='jsend') {
            try {
              response = JSON.parse(response);
            } catch (e) {
              return promise.reject(response);
            }
            if (response && response.status == 'success') {
              return promise.resolve({data:response.data, response: response});
            } else {
              return promise.reject(response);
            }
          } else {
            return promise.resolve({data:req.responseText, response:req});
          }
        },
        handleProgress = function(e) {
          var percentComplete = 0;
          if (e.lengthComputable) {
            percentComplete = e.loaded / e.total;
          }
          if (options.progress) {
            options.progress({
              percent: percentComplete,
              event : e
            });
          }
        },
        handleError = (options.error && typeof options.error == 'function') ? options.error : function () {},
        send = function(data) {
          if (data && typeof data == 'object') {
            data = util.toQueryString(data);
          }
          if (data && method=='get') {
            url = url.replace(/\?(.*)+/,'?$1'+'&'+data);
          }
          req.open(method, url, async);
          req.setRequestHeader('X-Requested-With','XMLHttpRequest');

          if (method.toLowerCase() == 'post') {
            req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          }
          for (var key in options.headers) {
            req.setRequestHeader(key, options.headers[key]);
          }
          if (options.progress) {
            req.onprogress=handleProgress;
          }
          req.onreadystatechange = function hdl(){
            if(req.readyState==4) {
              running = false;
              if (options.progress) {
                options.progress({
                  percent: 100
                });
              }
              if((/^[20]/).test(req.status)) {
                handleResp();
              }
              if((/^[45]/).test(req.status)) {
                handleError();
              }
            }
          };
          running = true;
          req.send(data||params);
        },
        cancel = function() {
          if (!running) {
            return;
          }
          running = false;
          req.abort();
          req.onreadystatechange = function(){};
          promise.reject('cancel');
        };
      promise.then(function(data) {
        options.success&&options.success(data.data, data.response);
      }, 
      function(data) {
        if ((options.format||'text').toLowerCase()=='jsend') {
          return options[data.status] && options[data.status](data);
        }
        options.error&&options.error(data);
      });
      if (method=='get' && !(/\?/.test(url))) {
        url += '?';
      }
      if (options.send) {
        setTimeout(function() {
          send(params);
        }, 0);
      }

      promise.xhr = req;
      promise.send = send;
      promise.isRunning = function(){return isRunning;};
      promise.cancel = cancel;
      return promise;
    };
  return {
    get:function(url, opts) {
      opts = opts||{};
      opts.method = 'get';
      opts.send = opts.send||true;
      return xhr(url, opts);
    },
    post:function(url, opts) {
      opts = opts||{};
      opts.method = 'post';
      opts.send = opts.send||true;
      return xhr(url, opts);
    }
  };
});

define('Buri/jsonp', ['Buri/util', 'Buri/dom', 'Buri/promise'], function(util, dom, Promise){
  "use strict";
  var id = 0,
    head = dom.select("head");

  return {
    get:function(url, options) {
      id++;
      var
        promise = new Promise(),
        data = options.data || {},
        key = options.key || 'callback',
        clbName = 'buri_jsonp_clb_'+id,
        timeout = options.timeout || 10,
        timeoutTimer,
        running = false,
        script = document.createElement('script');

      promise.then(options.success || function(){}, options.fail||options.error);

      data[key] = clbName;
      script.type = "text/javascript";
      script.async = true;
      script.src = url + (url.indexOf("?")+1 ? "&" : "?") + util.toQueryString(data);
      window[clbName] = function(json) {
        if (!running) {
          return;
        }
        running = false;
        head.removeChild(script);
        delete window[clbName];
        if (timeoutTimer) {
          clearTimeout(timeoutTimer);
          if (options.format == 'jsend') {
            if (json.status != 'success') {
              return promise.reject(json.data||json);
            }
          }
          promise.resolve(json);
        }
      };

      timeoutTimer = window.setTimeout(function(){
        running = false;
        promise.reject({message:'timeout'});
      }, timeout * 1000);
      running = true;
      head.appendChild(script);

      promise.isRunning = function() {
        return running;
      };
      promise.cancel = function() {
        running = false;
        head.removeChild(script);
        script=null;
        delete window[clbName];
        if (timeoutTimer) {
          clearTimeout(timeoutTimer);
        }
        promise.reject({'message':'cancel'});
      };

      return promise;
    }
  };
});
define('Buri/dom/live', ['Buri/dom'], function(dom){
  "use strict";
  var 
    doc = document,
    events = {},
    selectors = {},
    styles = doc.createElement('style'),
    keyframes = doc.createElement('style'),
    head = doc.getElementsByTagName('head')[0],
    startNames = ['animationstart', 'oAnimationStart', 'MSAnimationStart', 'webkitAnimationStart'],
    startEvent = function(event){
      event.selector = (events[event.animationName] || {}).selector;
      ((this.selectorListeners || {})[event.animationName] || []).forEach(function(fn){
        fn.call(this, event);
      }, this);
    },
    prefix = (function() {
      var duration = 'animation-duration: 0.01s;',
        name = 'animation-name: SelectorListener !important;',
        computed = window.getComputedStyle(doc.documentElement, ''),
        pre = (Array.prototype.slice.call(computed).join('').match(/moz|webkit|ms/)||(computed.OLink===''&&['o']))[0];
      return {
        css: '-' + pre + '-',
        properties: '{' + duration + name + '-' + pre + '-' + duration + '-' + pre + '-' + name + '}',
        keyframes: !!(window.CSSKeyframesRule || window[('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1] + 'CSSKeyframesRule'])
      };
    })();
    
  styles.type = keyframes.type = "text/css";
  head.appendChild(styles);
  head.appendChild(keyframes);
  
  dom.on = function(rootNode, selector, fn){
    var key = selectors[selector],
      listeners = rootNode.selectorListeners = rootNode.selectorListeners || {};
      
    if (key) {
      events[key].count++;
    }
    else {
      key = selectors[selector] = 'SelectorListener-' + new Date().getTime();
      var node = doc.createTextNode('@' + (prefix.keyframes ? prefix.css : '') + 'keyframes ' + key + ' {' +
        'from { clip: rect(1px, auto, auto, auto); } to { clip: rect(0px, auto, auto, auto); }' +
       '}');
      keyframes.appendChild(node);
      styles.sheet.insertRule(selector + prefix.properties.replace(/SelectorListener/g, key), 0);
      events[key] = { count: 1, selector: selector, keyframe: node, rule: styles.sheet.cssRules[0] };
    } 
    
    if (listeners.count) {
      listeners.count++;
    }
    else {
      listeners.count = 1;
      startNames.forEach(function(name){
        rootNode.addEventListener(name, startEvent, false);
      });
    }
    
    (listeners[key] = listeners[key] || []).push(fn);
  };
  
  dom.off = function(rootNode, selector, fn){
    var listeners = rootNode.selectorListeners || {},
      key = selectors[selector],
      listener = listeners[key] || [],
      index = listener.indexOf(fn);
      
    if (index > -1){
      var event = events[selectors[selector]];
      event.count--;
      if (!event.count){
        styles.sheet.deleteRule(styles.sheet.cssRules.item(event.rule));
        keyframes.removeChild(event.keyframe);
        delete events[key];
        delete selectors[selector];
      }
      
      listeners.count--;
      listener.splice(index, 1);
      if (!listeners.count) {
        startNames.forEach(function(name){
          rootNode.removeEventListener(name, startEvent, false);
        });
      }
    }
  };
  
});
