
define('Buri/class', ['Buri/util'], function(util){
  "use strict";
  var 
    defineProperty = Object.defineProperty,
    getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

  try {
    defineProperty({}, "~", {});
    getOwnPropertyDescriptor({}, "~");
  } catch (e){
    defineProperty = null;
    getOwnPropertyDescriptor = null;
  }

  var define = function(value, key, from){
    defineProperty(this, key, getOwnPropertyDescriptor(from, key) || {
      writable: true,
      enumerable: true,
      configurable: true,
      value: value
    });
  };
  var implement = function(proto){
    util.forIn(proto, defineProperty ? define : copyProp, this.prototype);
    return this;
  };
  var 
    mixIn = function(target, objects){
      var 
        i = 0,
        n = arguments.length,
        obj;
      while(++i < n){
        obj = arguments[i];
        if (obj !== null) {
          util.forIn(obj, function(val, key) {
            if (util.hasOwn(obj, key)) {
              return copyProp.call(target, obj[key], key, obj);
            }
          });
        }
      }
      return target;
    },
    copyProp = function(val, key){
      this[key] = val;
    },
    create = function(parent, props) {
      function F(){}
      F.prototype = parent;
      return mixIn(new F(), props);
    },
    verbs = /^constructor|inherits|mixin$/,
    klass = function(proto) {
      var superklass = proto.inherits;
      var constructor = (util.hasOwn(proto, "constructor")) ? proto.constructor : (superklass) ? function(){
        return superklass.apply(this, arguments);
      } : function(){};

      if (superklass){
        mixIn(constructor, superklass);

        var superproto = superklass.prototype;
        // inherit from superprime
        var cproto = constructor.prototype = create(superproto);

        // setting constructor.parent to superprime.prototype
        // because it's the shortest possible absolute reference
        constructor.parent = superproto;
        cproto.constructor = constructor;
      }

      if (!constructor.implement) {
        constructor.implement = implement;
      }

      // implement proto and return constructor
      return constructor.implement(util.filter(proto, function(value, key){
        return !key.match(verbs);
      }));
    };
  return klass;
});