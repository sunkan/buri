define('Buri/dev',[], function(){
  "use strict";
  var 
    history = [],
    con = window.console||false;

  return {
    log : function(){
      history.push(arguments);
      if (con) {
        con.log[ con.firebug ? 'apply' : 'call']( con, Array.prototype.slice.call( arguments ) );
      }
    },
    history : function() {
      return history;
    }
  };
});
